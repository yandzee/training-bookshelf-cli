const CLIInterface = require('./cli');

const PrintCommand = require('./commands/print');
const DeleteCommand = require('./commands/delete');
const AddCommand = require('./commands/add');
const ExitCommand = require('./commands/exit');

class App {
  constructor() {
    this.commands = this.prepareCommands();
    this.currentCommand = null;

    this.cli = new CLIInterface(this.usage.description, this.usage.commands);
    this.setupEventHandling();
  }

  get shared() {
    return {};
  }

  setupEventHandling() {
    this.cli.on('line', async (line) => {
      await this.advanceInput(line);
    });
  }

  async advanceInput(line) {
    if (this.currentCommand != null) {
      const finished = await this.currentCommand.advance(line);

      if (finished) {
        this.currentCommand = null;
      }

      return;
    }

    switch (line.trim()) {
      case '1':
        this.currentCommand = this.commands.print;
        break;
      case '2':
        this.currentCommand = this.commands.delete;
        break;
      case '3':
        this.currentCommand = this.commands.add;
        break;
      case '4':
        this.currentCommand = this.commands.exit;
        break;
      default:
        console.log(`I dont have command '${line.trim()}'`);
        this.cli.printUsage();
        break;
    }

    // We have just setup the command
    const finished = this.currentCommand.init(this.shared);
    if (finished) {
      this.currentCommand = null;
    }
  }

  prepareCommands() {
    return {
      print: new PrintCommand(),
      delete: new DeleteCommand(),
      add: new AddCommand(),
      exit: new ExitCommand(),
    };
  }

  run() {
    this.cli.printUsage();
    this.cli.run();
  }

  get usage() {
    return {
      description: (
        'Hello! This is a small cli program that allows you to store and query' +
        'your books :)'
      ),
      commands: [
        this.commands.print.description,
        this.commands.delete.description,
        this.commands.add.description,
        this.commands.exit.description,
      ]
    }
  }
}

module.exports = App;
