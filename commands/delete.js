const CommandBase = require('./base');

class DeleteCommand extends CommandBase {
  advance() {
    console.log('about to delete some books');
    return true;
  }

  init(shared) {
    super.init(shared);

    return this.advance();
  }

  get description() {
    return 'Delete books.';
  }
}

module.exports = DeleteCommand;
