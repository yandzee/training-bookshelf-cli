const CommandBase = require('./base');

class ExitCommand extends CommandBase {
  advance() {
    console.log('Have a nice day!');
    process.exit(0);
  }

  get description() {
    return 'Exit program.';
  }

  init(shared) {
    super.init(shared);

    this.advance();
  }
}

module.exports = ExitCommand;
