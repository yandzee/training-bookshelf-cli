class CommandBase {
  constructor() {
    this.shared = null;
  }

  init(shared) {
    this.shared = shared;
  }
}

module.exports = CommandBase;
