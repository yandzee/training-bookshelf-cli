const CommandBase = require('./base');

class PrintCommand extends CommandBase {
  advance() {
    this.printBooks();
    return true;
  }

  printBooks() {
    console.log('Here is your book list: ');
  }

  init(shared) {
    super.init(shared);

    return this.advance();
  }

  get description() {
    return 'Print books.';
  }
}

module.exports = PrintCommand;
