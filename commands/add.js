const CommandBase = require('./base');

class AddCommand extends CommandBase {
  advance() {
    console.log('about to add new book');
    return true;
  }

  init(shared) {
    super.init(shared);

    return this.advance();
  }

  get description() {
    return 'Add new book.';
  }
}

module.exports = AddCommand;
