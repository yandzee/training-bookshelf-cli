const EventEmitter = require('events');
const rl = require('readline');

class CLIInterface extends EventEmitter {
  static get promptChar() {
    return '> ';
  }

  constructor(desc, commands) {
    super();

    this.usage = this.buildUsage(desc, commands);
    this.rl = rl.createInterface({
      input: process.stdin,
      output: process.stdout,
      prompt: CLIInterface.promptChar,
    });
  }

  run() {
    this.rl.prompt();

    this.rl.on('line', async line => {
      this.emit('line', line);

      this.rl.prompt();
    });
  }

  readCommand(commandHandler) {
    this.rl.prompt();

    this.rl.question('', commandHandler);
  }

  printUsage() {
    console.log(this.usage);
  }

  buildUsage(desc, commands) {
    return (
      `${desc}.\nPlease choose one of the following command and type it's` +
      `number right after \`${CLIInterface.promptChar.trim()}\` sign:\n\n` +
      `${commands.map((cmd, idx) => `${idx + 1}. ${cmd}\n`).join('')}\n`
    );
  }
}

module.exports = CLIInterface;
